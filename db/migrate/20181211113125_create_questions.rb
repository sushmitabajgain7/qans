class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :title, default: "", null: false
      t.text :description, null: false
      t.string :city, null: false
      t.integer :user_id, null: false
      t.json :tags, null: false
      t.timestamps
    end
  end
end